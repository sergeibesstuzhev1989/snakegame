// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BonusBase.h"
#include "BonusSpeed.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API ABonusSpeed : public ABonusBase
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	float BonusPower;

	virtual void BonusAction(ASnakeBase* Snake) override;
	
};
