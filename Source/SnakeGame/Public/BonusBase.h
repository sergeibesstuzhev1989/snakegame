// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.h"

#include "BonusBase.generated.h"

// объявляем свой делегат
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FMyBaseDelegate);

UCLASS()
class SNAKEGAME_API ABonusBase : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABonusBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	UFUNCTION()
	virtual void BonusAction(ASnakeBase* Snake);

	UFUNCTION()
	void AutoDestroy();

	// объявляем свой делегат
	FMyBaseDelegate BonusBaseActivated;

};
