// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BonusBase.h"
#include "Food.generated.h"

// объявляем свой делегат 
//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FMyDelegate);

UCLASS()
class SNAKEGAME_API AFood : public ABonusBase
{
	GENERATED_BODY()

	virtual void BonusAction(ASnakeBase* Snake) override;
	
};
