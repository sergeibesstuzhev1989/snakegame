// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "SnakeGame_GameInstance.generated.h"

class USaveGame;

UCLASS()
class SNAKEGAME_API USnakeGame_GameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	virtual void Init() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Save")
	int32 Record = 0;

	UFUNCTION(BlueprintCallable, Category = "Save")
	void SetNewRecord(int32 NewRecord);
	
	UFUNCTION()
	void LoadRecord(const FString& SlotName, const int32 UserIndex, USaveGame* LoadedGame);

	UFUNCTION()
	void SaveRecord(const FString& SlotName, const int32 UserIndex, USaveGame* LoadedGame);
};
