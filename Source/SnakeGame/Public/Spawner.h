// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Spawner.generated.h"

class AFood;
class ABonusBase;
class ABonusBlock;
class AWall;
class UAudioComponent;

UCLASS()
class SNAKEGAME_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	TArray<FVector> LocationForBonus;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly)
	UAudioComponent* MyAudioComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ABonusBlock> BlockActor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<TSubclassOf<ABonusBase>> BonusTypeArray;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 NumberOfBlocks = 5;
	
	UFUNCTION()
	void SpawnBlock();

	UFUNCTION()
	void SpawnBonus();
};
