// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "SnakeGame_SaveGame.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API USnakeGame_SaveGame : public USaveGame
{
	GENERATED_BODY()

public:
	// UPROPERTY() обязателен
	UPROPERTY()
	int32 SaveRecordScore = 0;
};
