// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BonusBase.h"

#include "BonusBlock.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API ABonusBlock : public ABonusBase
{
	GENERATED_BODY()

public:
	ABonusBlock();

protected:
	virtual void BeginPlay() override;

public:
	virtual void BonusAction(ASnakeBase* Snake) override;
};
