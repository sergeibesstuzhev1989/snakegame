// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"

void AFood::BonusAction(ASnakeBase* Snake)
{
	Super::BonusAction(Snake);

	Snake->AddSnakeElement();

	Snake->Points += 10;

	Destroy();
	
}

