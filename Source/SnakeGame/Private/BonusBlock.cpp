// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusBlock.h"

ABonusBlock::ABonusBlock()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ABonusBlock::BeginPlay()
{
	/**/
}

void ABonusBlock::BonusAction(ASnakeBase* Snake)
{
	Super::BonusAction(Snake);

	Snake->Life -= 1;
	if(Snake->Life <= 0)
	{
		Snake->KillSnake();
	}

	Destroy();
}



