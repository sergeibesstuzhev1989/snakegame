// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusLife.h"

void ABonusLife::BonusAction(ASnakeBase* Snake)
{
	Super::BonusAction(Snake);

	Snake->Life += 1;

	Snake->Points += 10;

	Destroy();
}
