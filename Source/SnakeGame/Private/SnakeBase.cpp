// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeGame/Public/SnakeElementBase.h"
#include "Interactable.h"
#include "SnakeGame_GameInstance.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMovementDirection = EMovementDirection::LEFT;
	Life = 3;
	bMoving = false;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElement(3);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetActorTickInterval(MovementSpeed);
	Move();

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 65);
		FTransform NewTransform = FTransform(NewLocation);

		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;
		int32 ElementIndex = SnakeElements.Add(NewSnakeElement);
		Points += DefaultPoints;
		if(ElementIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
		}
		else
		{
			NewSnakeElement->MeshComponent->SetVisibility(false);
			SetActorTickInterval(MovementSpeed);
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector;
	float NewMovementSpeed = ElementSize;
	switch (LastMovementDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += NewMovementSpeed;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= NewMovementSpeed;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += NewMovementSpeed;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= NewMovementSpeed;
		break;
	}

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		CurrentElement->MeshComponent->SetVisibility(true);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	bMoving = false;
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if(IsValid(OverlappedElement))
	{
		int32 ElementIndex;
		SnakeElements.Find(OverlappedElement, ElementIndex);
		bool bIsFirst = ElementIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);

		if(InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::KillSnake()
{
	for(auto elem : SnakeElements)
	{
		elem->Destroy();
	}

	if(auto MyGameInstance = Cast<USnakeGame_GameInstance>(UGameplayStatics::GetGameInstance(GetWorld())))
	{
		MyGameInstance->SetNewRecord(Points);
	}
	
	Destroy();
}
