// Fill out your copyright notice in the Description page of Project Settings.


#include "Teleport.h"
#include "Components/BoxComponent.h"

// Sets default values
ATeleport::ATeleport()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("MyBoxCollision"));
	Box->SetupAttachment(RootComponent);
	Box->SetBoxExtent(FVector(50,50,50), true);
	Box->SetRelativeScale3D(FVector(1.f,1.f,1.f));
	Box->OnComponentBeginOverlap.AddDynamic(this, &ATeleport::MyBeginOverlap);
	Box->OnComponentEndOverlap.AddDynamic(this, &ATeleport::MyEndOverlap);

}

// Called when the game starts or when spawned
void ATeleport::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATeleport::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATeleport::MyBeginOverlap(UPrimitiveComponent* OverlappedComp,
							   AActor* OtherActor,
							   UPrimitiveComponent* OtherComp,
							   int32 OtherBodyIndex,
							   bool bFromSweep,
							   const FHitResult& SweepResult)
{
	FVector ExitLocation = GetActorLocation();
	if (bYTeleport)
	{
		if (ExitLocation.Y > 0)
		{
			ExitLocation.Y = (ExitLocation.Y - 60) * -1;
		}
		else
		{
			ExitLocation.Y = (ExitLocation.Y + 60) * -1;
		}
	}
	else
	{
		// int a = b > 0 ? 1 : -1;
		ExitLocation.X = (ExitLocation.X > 0 ? ExitLocation.X - 60 : ExitLocation.X + 60) * -1;
	}

	//      
	OtherActor->SetActorLocation(ExitLocation, false, nullptr, ETeleportType::None);

}

void ATeleport::MyEndOverlap(UPrimitiveComponent* OverlappedComp,
							 AActor* OtherActor,
							 UPrimitiveComponent* OtherComp,
                             int32 OtherBodyIndex)
{
	//
}

