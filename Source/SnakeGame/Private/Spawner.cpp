// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"
#include "BonusBase.h"
#include "BonusBlock.h"
#include "Components/AudioComponent.h"

// Sets default values
ASpawner::ASpawner() //: FoodClass(nullptr) 
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MyAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
	MyAudioComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	MyAudioComponent->bAutoActivate = false;
}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	
	SpawnBlock();
	SpawnBonus();
	
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASpawner::SpawnBlock()
{
	// спавним несколько припятствий на уровне
	for (int32 i = 0; i < NumberOfBlocks; i++)
	{
		int32 RandomX = FMath::RandRange(-1000, 1000);
		int32 RandomY = FMath::RandRange(-1000, 1000);
		auto SpawnLocation = FVector(RandomX, RandomY, 65);
		
		GetWorld()->SpawnActor<ABonusBlock>(BlockActor, FTransform(SpawnLocation));
	}

}

void ASpawner::SpawnBonus()
{
	// если массив бонусов не пустой
	if (BonusTypeArray.Num())
	{
		// выбираем рандомный бонус и локацию
		int32 RandomBonusIndex = FMath::RandRange(0,BonusTypeArray.Num() - 1);
		auto ClassForSpawn = BonusTypeArray[RandomBonusIndex];
		int32 RandomX = FMath::RandRange(-1000, 1000);
		int32 RandomY = FMath::RandRange(-1000, 1000);
		auto SpawnLocation = FVector(RandomX, RandomY, 65);

		// спавним бонус
		if(ClassForSpawn)
		{
			auto CurrentBonus = GetWorld()->SpawnActor<ABonusBase>(ClassForSpawn, FTransform(SpawnLocation));
			// устанавливаю время жизни бонуса
			//CurrentBonus->SetLifeSpan(10.0f);
			// подвязываюю на активацию бонуса спавн нового бонуса
			CurrentBonus->BonusBaseActivated.AddDynamic(this, &ASpawner::SpawnBonus);
		}
		// немного дебага
		// FString DebugMessage = TEXT("Bonus name: ") + CurrentBonus->GetName();
		// GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, *DebugMessage);

		if(MyAudioComponent)
		{
			MyAudioComponent->Play();
		}
	}

}

