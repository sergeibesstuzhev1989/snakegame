// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeGame_GameInstance.h"
#include "SnakeGame_SaveGame.h"
#include "GameFramework/SaveGame.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/GameUserSettings.h" // для работы с настройками

void USnakeGame_GameInstance::Init()
{
	Super::Init();

	GEngine->GameUserSettings->SetVSyncEnabled(true);
	GEngine->GameUserSettings->ApplySettings(true);
	GEngine->GameUserSettings->SaveSettings();
	GEngine->Exec(GetWorld(), TEXT("t.MaxFPS 60"));

	// запускаем асинхронную загрузку (бл*т!)
	// привязываем свою функцию к делегату, загрузка закончена
	const FAsyncLoadGameFromSlotDelegate OnLoaded = FAsyncLoadGameFromSlotDelegate::CreateUObject(this, &USnakeGame_GameInstance::LoadRecord);
	// загружаем файл сохранения
	UGameplayStatics::AsyncLoadGameFromSlot("RecordScore", 0, OnLoaded);
}

void USnakeGame_GameInstance::SetNewRecord(int32 NewRecord)
{
	if(NewRecord > Record)
	{
		Record = NewRecord;
		if(UGameplayStatics::DoesSaveGameExist("RecordScore", 0)) // существует ли такое сохранение?!
		{
			const FAsyncLoadGameFromSlotDelegate OnLoaded = FAsyncLoadGameFromSlotDelegate::CreateUObject(this, &USnakeGame_GameInstance::SaveRecord);
			UGameplayStatics::AsyncLoadGameFromSlot("RecordScore", 0, OnLoaded);
		}
		else
		{
			USnakeGame_SaveGame* SGame = Cast<USnakeGame_SaveGame>(UGameplayStatics::CreateSaveGameObject(USnakeGame_SaveGame::StaticClass()));
			if(SGame)
			{
				SGame->SaveRecordScore = Record;
				UGameplayStatics::AsyncSaveGameToSlot(SGame, "RecordScore", 0);
			}
		}
	}
}

void USnakeGame_GameInstance::LoadRecord(const FString& SlotName, const int32 UserIndex, USaveGame* LoadedGame)
{
	USnakeGame_SaveGame* LoadSaveGame = Cast<USnakeGame_SaveGame>(LoadedGame);
	if(LoadSaveGame)
	{
		Record = LoadSaveGame->SaveRecordScore;
	}
}

void USnakeGame_GameInstance::SaveRecord(const FString& SlotName, const int32 UserIndex, USaveGame* LoadedGame)
{
	USnakeGame_SaveGame* SGame = Cast<USnakeGame_SaveGame>(LoadedGame);
	if(SGame)
	{
		SGame->SaveRecordScore = Record;
		
	}
	UGameplayStatics::AsyncSaveGameToSlot(SGame, "RecordScore", 0);
}
