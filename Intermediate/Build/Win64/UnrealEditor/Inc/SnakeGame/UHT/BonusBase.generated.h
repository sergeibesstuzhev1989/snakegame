// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "BonusBase.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ASnakeBase;
#ifdef SNAKEGAME_BonusBase_generated_h
#error "BonusBase.generated.h already included, missing '#pragma once' in BonusBase.h"
#endif
#define SNAKEGAME_BonusBase_generated_h

#define FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_13_DELEGATE \
static inline void FMyBaseDelegate_DelegateWrapper(const FMulticastScriptDelegate& MyBaseDelegate) \
{ \
	MyBaseDelegate.ProcessMulticastDelegate<UObject>(NULL); \
}


#define FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_18_SPARSE_DATA
#define FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAutoDestroy); \
	DECLARE_FUNCTION(execBonusAction);


#define FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAutoDestroy); \
	DECLARE_FUNCTION(execBonusAction);


#define FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_18_ACCESSORS
#define FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABonusBase(); \
	friend struct Z_Construct_UClass_ABonusBase_Statics; \
public: \
	DECLARE_CLASS(ABonusBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ABonusBase) \
	virtual UObject* _getUObject() const override { return const_cast<ABonusBase*>(this); }


#define FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_18_INCLASS \
private: \
	static void StaticRegisterNativesABonusBase(); \
	friend struct Z_Construct_UClass_ABonusBase_Statics; \
public: \
	DECLARE_CLASS(ABonusBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ABonusBase) \
	virtual UObject* _getUObject() const override { return const_cast<ABonusBase*>(this); }


#define FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABonusBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABonusBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABonusBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABonusBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABonusBase(ABonusBase&&); \
	NO_API ABonusBase(const ABonusBase&); \
public: \
	NO_API virtual ~ABonusBase();


#define FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABonusBase(ABonusBase&&); \
	NO_API ABonusBase(const ABonusBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABonusBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABonusBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABonusBase) \
	NO_API virtual ~ABonusBase();


#define FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_15_PROLOG
#define FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_18_SPARSE_DATA \
	FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_18_RPC_WRAPPERS \
	FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_18_ACCESSORS \
	FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_18_INCLASS \
	FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_18_SPARSE_DATA \
	FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_18_ACCESSORS \
	FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_18_INCLASS_NO_PURE_DECLS \
	FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class ABonusBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
