// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "SnakeGame_GameInstance.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class USaveGame;
#ifdef SNAKEGAME_SnakeGame_GameInstance_generated_h
#error "SnakeGame_GameInstance.generated.h already included, missing '#pragma once' in SnakeGame_GameInstance.h"
#endif
#define SNAKEGAME_SnakeGame_GameInstance_generated_h

#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_14_SPARSE_DATA
#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSaveRecord); \
	DECLARE_FUNCTION(execLoadRecord); \
	DECLARE_FUNCTION(execSetNewRecord);


#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSaveRecord); \
	DECLARE_FUNCTION(execLoadRecord); \
	DECLARE_FUNCTION(execSetNewRecord);


#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_14_ACCESSORS
#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSnakeGame_GameInstance(); \
	friend struct Z_Construct_UClass_USnakeGame_GameInstance_Statics; \
public: \
	DECLARE_CLASS(USnakeGame_GameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(USnakeGame_GameInstance)


#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUSnakeGame_GameInstance(); \
	friend struct Z_Construct_UClass_USnakeGame_GameInstance_Statics; \
public: \
	DECLARE_CLASS(USnakeGame_GameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(USnakeGame_GameInstance)


#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USnakeGame_GameInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USnakeGame_GameInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USnakeGame_GameInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USnakeGame_GameInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USnakeGame_GameInstance(USnakeGame_GameInstance&&); \
	NO_API USnakeGame_GameInstance(const USnakeGame_GameInstance&); \
public: \
	NO_API virtual ~USnakeGame_GameInstance();


#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USnakeGame_GameInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USnakeGame_GameInstance(USnakeGame_GameInstance&&); \
	NO_API USnakeGame_GameInstance(const USnakeGame_GameInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USnakeGame_GameInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USnakeGame_GameInstance); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USnakeGame_GameInstance) \
	NO_API virtual ~USnakeGame_GameInstance();


#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_11_PROLOG
#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_14_SPARSE_DATA \
	FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_14_RPC_WRAPPERS \
	FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_14_ACCESSORS \
	FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_14_INCLASS \
	FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_14_SPARSE_DATA \
	FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_14_ACCESSORS \
	FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_14_INCLASS_NO_PURE_DECLS \
	FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class USnakeGame_GameInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
