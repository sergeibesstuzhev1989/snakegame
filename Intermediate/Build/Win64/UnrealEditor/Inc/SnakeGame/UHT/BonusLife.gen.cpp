// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/Public/BonusLife.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBonusLife() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_ABonusBase();
	SNAKEGAME_API UClass* Z_Construct_UClass_ABonusLife();
	SNAKEGAME_API UClass* Z_Construct_UClass_ABonusLife_NoRegister();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
// End Cross Module References
	void ABonusLife::StaticRegisterNativesABonusLife()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ABonusLife);
	UClass* Z_Construct_UClass_ABonusLife_NoRegister()
	{
		return ABonusLife::StaticClass();
	}
	struct Z_Construct_UClass_ABonusLife_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABonusLife_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ABonusBase,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABonusLife_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "BonusLife.h" },
		{ "ModuleRelativePath", "Public/BonusLife.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABonusLife_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABonusLife>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ABonusLife_Statics::ClassParams = {
		&ABonusLife::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABonusLife_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABonusLife_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABonusLife()
	{
		if (!Z_Registration_Info_UClass_ABonusLife.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ABonusLife.OuterSingleton, Z_Construct_UClass_ABonusLife_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ABonusLife.OuterSingleton;
	}
	template<> SNAKEGAME_API UClass* StaticClass<ABonusLife>()
	{
		return ABonusLife::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABonusLife);
	ABonusLife::~ABonusLife() {}
	struct Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_BonusLife_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_BonusLife_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ABonusLife, ABonusLife::StaticClass, TEXT("ABonusLife"), &Z_Registration_Info_UClass_ABonusLife, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ABonusLife), 1834946668U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_BonusLife_h_1765947551(TEXT("/Script/SnakeGame"),
		Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_BonusLife_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_BonusLife_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
