// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/Public/BonusBlock.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBonusBlock() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_ABonusBase();
	SNAKEGAME_API UClass* Z_Construct_UClass_ABonusBlock();
	SNAKEGAME_API UClass* Z_Construct_UClass_ABonusBlock_NoRegister();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
// End Cross Module References
	void ABonusBlock::StaticRegisterNativesABonusBlock()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ABonusBlock);
	UClass* Z_Construct_UClass_ABonusBlock_NoRegister()
	{
		return ABonusBlock::StaticClass();
	}
	struct Z_Construct_UClass_ABonusBlock_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABonusBlock_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ABonusBase,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABonusBlock_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "BonusBlock.h" },
		{ "ModuleRelativePath", "Public/BonusBlock.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABonusBlock_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABonusBlock>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ABonusBlock_Statics::ClassParams = {
		&ABonusBlock::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABonusBlock_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABonusBlock_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABonusBlock()
	{
		if (!Z_Registration_Info_UClass_ABonusBlock.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ABonusBlock.OuterSingleton, Z_Construct_UClass_ABonusBlock_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ABonusBlock.OuterSingleton;
	}
	template<> SNAKEGAME_API UClass* StaticClass<ABonusBlock>()
	{
		return ABonusBlock::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABonusBlock);
	ABonusBlock::~ABonusBlock() {}
	struct Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_BonusBlock_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_BonusBlock_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ABonusBlock, ABonusBlock::StaticClass, TEXT("ABonusBlock"), &Z_Registration_Info_UClass_ABonusBlock, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ABonusBlock), 2744255391U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_BonusBlock_h_1689776339(TEXT("/Script/SnakeGame"),
		Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_BonusBlock_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_BonusBlock_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
