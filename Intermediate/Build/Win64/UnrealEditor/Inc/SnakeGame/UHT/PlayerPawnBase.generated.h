// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "PlayerPawnBase.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_PlayerPawnBase_generated_h
#error "PlayerPawnBase.generated.h already included, missing '#pragma once' in PlayerPawnBase.h"
#endif
#define SNAKEGAME_PlayerPawnBase_generated_h

#define FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_18_SPARSE_DATA
#define FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHandlePlayerHorizontalInput); \
	DECLARE_FUNCTION(execHandlePlayerVerticalInput); \
	DECLARE_FUNCTION(execCreateSnakeActor);


#define FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHandlePlayerHorizontalInput); \
	DECLARE_FUNCTION(execHandlePlayerVerticalInput); \
	DECLARE_FUNCTION(execCreateSnakeActor);


#define FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_18_ACCESSORS
#define FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerPawnBase(); \
	friend struct Z_Construct_UClass_APlayerPawnBase_Statics; \
public: \
	DECLARE_CLASS(APlayerPawnBase, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(APlayerPawnBase)


#define FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerPawnBase(); \
	friend struct Z_Construct_UClass_APlayerPawnBase_Statics; \
public: \
	DECLARE_CLASS(APlayerPawnBase, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(APlayerPawnBase)


#define FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerPawnBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerPawnBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPawnBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPawnBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPawnBase(APlayerPawnBase&&); \
	NO_API APlayerPawnBase(const APlayerPawnBase&); \
public: \
	NO_API virtual ~APlayerPawnBase();


#define FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPawnBase(APlayerPawnBase&&); \
	NO_API APlayerPawnBase(const APlayerPawnBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPawnBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPawnBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerPawnBase) \
	NO_API virtual ~APlayerPawnBase();


#define FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_15_PROLOG
#define FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_18_SPARSE_DATA \
	FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_18_RPC_WRAPPERS \
	FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_18_ACCESSORS \
	FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_18_INCLASS \
	FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_18_SPARSE_DATA \
	FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_18_ACCESSORS \
	FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_18_INCLASS_NO_PURE_DECLS \
	FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class APlayerPawnBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_SnakeGame_Source_SnakeGame_Public_PlayerPawnBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
