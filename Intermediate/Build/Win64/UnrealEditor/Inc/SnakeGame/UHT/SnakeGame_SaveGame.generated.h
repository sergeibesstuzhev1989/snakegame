// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "SnakeGame_SaveGame.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_SnakeGame_SaveGame_generated_h
#error "SnakeGame_SaveGame.generated.h already included, missing '#pragma once' in SnakeGame_SaveGame.h"
#endif
#define SNAKEGAME_SnakeGame_SaveGame_generated_h

#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_15_SPARSE_DATA
#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_15_RPC_WRAPPERS
#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_15_ACCESSORS
#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSnakeGame_SaveGame(); \
	friend struct Z_Construct_UClass_USnakeGame_SaveGame_Statics; \
public: \
	DECLARE_CLASS(USnakeGame_SaveGame, USaveGame, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(USnakeGame_SaveGame)


#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUSnakeGame_SaveGame(); \
	friend struct Z_Construct_UClass_USnakeGame_SaveGame_Statics; \
public: \
	DECLARE_CLASS(USnakeGame_SaveGame, USaveGame, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(USnakeGame_SaveGame)


#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USnakeGame_SaveGame(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USnakeGame_SaveGame) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USnakeGame_SaveGame); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USnakeGame_SaveGame); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USnakeGame_SaveGame(USnakeGame_SaveGame&&); \
	NO_API USnakeGame_SaveGame(const USnakeGame_SaveGame&); \
public: \
	NO_API virtual ~USnakeGame_SaveGame();


#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USnakeGame_SaveGame(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USnakeGame_SaveGame(USnakeGame_SaveGame&&); \
	NO_API USnakeGame_SaveGame(const USnakeGame_SaveGame&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USnakeGame_SaveGame); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USnakeGame_SaveGame); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USnakeGame_SaveGame) \
	NO_API virtual ~USnakeGame_SaveGame();


#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_12_PROLOG
#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_15_SPARSE_DATA \
	FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_15_RPC_WRAPPERS \
	FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_15_ACCESSORS \
	FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_15_INCLASS \
	FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_15_SPARSE_DATA \
	FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_15_ACCESSORS \
	FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_15_INCLASS_NO_PURE_DECLS \
	FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class USnakeGame_SaveGame>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
