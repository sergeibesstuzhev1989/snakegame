// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/Public/SnakeGame_GameInstance.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSnakeGame_GameInstance() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_UGameInstance();
	ENGINE_API UClass* Z_Construct_UClass_USaveGame_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_USnakeGame_GameInstance();
	SNAKEGAME_API UClass* Z_Construct_UClass_USnakeGame_GameInstance_NoRegister();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
// End Cross Module References
	DEFINE_FUNCTION(USnakeGame_GameInstance::execSaveRecord)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_SlotName);
		P_GET_PROPERTY(FIntProperty,Z_Param_UserIndex);
		P_GET_OBJECT(USaveGame,Z_Param_LoadedGame);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SaveRecord(Z_Param_SlotName,Z_Param_UserIndex,Z_Param_LoadedGame);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USnakeGame_GameInstance::execLoadRecord)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_SlotName);
		P_GET_PROPERTY(FIntProperty,Z_Param_UserIndex);
		P_GET_OBJECT(USaveGame,Z_Param_LoadedGame);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->LoadRecord(Z_Param_SlotName,Z_Param_UserIndex,Z_Param_LoadedGame);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USnakeGame_GameInstance::execSetNewRecord)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_NewRecord);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetNewRecord(Z_Param_NewRecord);
		P_NATIVE_END;
	}
	void USnakeGame_GameInstance::StaticRegisterNativesUSnakeGame_GameInstance()
	{
		UClass* Class = USnakeGame_GameInstance::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "LoadRecord", &USnakeGame_GameInstance::execLoadRecord },
			{ "SaveRecord", &USnakeGame_GameInstance::execSaveRecord },
			{ "SetNewRecord", &USnakeGame_GameInstance::execSetNewRecord },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics
	{
		struct SnakeGame_GameInstance_eventLoadRecord_Parms
		{
			FString SlotName;
			int32 UserIndex;
			USaveGame* LoadedGame;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_SlotName_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_SlotName;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_UserIndex_MetaData[];
#endif
		static const UECodeGen_Private::FIntPropertyParams NewProp_UserIndex;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_LoadedGame;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::NewProp_SlotName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::NewProp_SlotName = { "SlotName", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(SnakeGame_GameInstance_eventLoadRecord_Parms, SlotName), METADATA_PARAMS(Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::NewProp_SlotName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::NewProp_SlotName_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::NewProp_UserIndex_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::NewProp_UserIndex = { "UserIndex", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(SnakeGame_GameInstance_eventLoadRecord_Parms, UserIndex), METADATA_PARAMS(Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::NewProp_UserIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::NewProp_UserIndex_MetaData)) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::NewProp_LoadedGame = { "LoadedGame", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(SnakeGame_GameInstance_eventLoadRecord_Parms, LoadedGame), Z_Construct_UClass_USaveGame_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::NewProp_SlotName,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::NewProp_UserIndex,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::NewProp_LoadedGame,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/SnakeGame_GameInstance.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USnakeGame_GameInstance, nullptr, "LoadRecord", nullptr, nullptr, sizeof(Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::SnakeGame_GameInstance_eventLoadRecord_Parms), Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics
	{
		struct SnakeGame_GameInstance_eventSaveRecord_Parms
		{
			FString SlotName;
			int32 UserIndex;
			USaveGame* LoadedGame;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_SlotName_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_SlotName;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_UserIndex_MetaData[];
#endif
		static const UECodeGen_Private::FIntPropertyParams NewProp_UserIndex;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_LoadedGame;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::NewProp_SlotName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::NewProp_SlotName = { "SlotName", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(SnakeGame_GameInstance_eventSaveRecord_Parms, SlotName), METADATA_PARAMS(Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::NewProp_SlotName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::NewProp_SlotName_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::NewProp_UserIndex_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::NewProp_UserIndex = { "UserIndex", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(SnakeGame_GameInstance_eventSaveRecord_Parms, UserIndex), METADATA_PARAMS(Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::NewProp_UserIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::NewProp_UserIndex_MetaData)) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::NewProp_LoadedGame = { "LoadedGame", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(SnakeGame_GameInstance_eventSaveRecord_Parms, LoadedGame), Z_Construct_UClass_USaveGame_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::NewProp_SlotName,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::NewProp_UserIndex,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::NewProp_LoadedGame,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/SnakeGame_GameInstance.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USnakeGame_GameInstance, nullptr, "SaveRecord", nullptr, nullptr, sizeof(Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::SnakeGame_GameInstance_eventSaveRecord_Parms), Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USnakeGame_GameInstance_SetNewRecord_Statics
	{
		struct SnakeGame_GameInstance_eventSetNewRecord_Parms
		{
			int32 NewRecord;
		};
		static const UECodeGen_Private::FIntPropertyParams NewProp_NewRecord;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_USnakeGame_GameInstance_SetNewRecord_Statics::NewProp_NewRecord = { "NewRecord", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(SnakeGame_GameInstance_eventSetNewRecord_Parms, NewRecord), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USnakeGame_GameInstance_SetNewRecord_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USnakeGame_GameInstance_SetNewRecord_Statics::NewProp_NewRecord,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USnakeGame_GameInstance_SetNewRecord_Statics::Function_MetaDataParams[] = {
		{ "Category", "Save" },
		{ "ModuleRelativePath", "Public/SnakeGame_GameInstance.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USnakeGame_GameInstance_SetNewRecord_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USnakeGame_GameInstance, nullptr, "SetNewRecord", nullptr, nullptr, sizeof(Z_Construct_UFunction_USnakeGame_GameInstance_SetNewRecord_Statics::SnakeGame_GameInstance_eventSetNewRecord_Parms), Z_Construct_UFunction_USnakeGame_GameInstance_SetNewRecord_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USnakeGame_GameInstance_SetNewRecord_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USnakeGame_GameInstance_SetNewRecord_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USnakeGame_GameInstance_SetNewRecord_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USnakeGame_GameInstance_SetNewRecord()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USnakeGame_GameInstance_SetNewRecord_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(USnakeGame_GameInstance);
	UClass* Z_Construct_UClass_USnakeGame_GameInstance_NoRegister()
	{
		return USnakeGame_GameInstance::StaticClass();
	}
	struct Z_Construct_UClass_USnakeGame_GameInstance_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Record_MetaData[];
#endif
		static const UECodeGen_Private::FIntPropertyParams NewProp_Record;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USnakeGame_GameInstance_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameInstance,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_USnakeGame_GameInstance_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_USnakeGame_GameInstance_LoadRecord, "LoadRecord" }, // 1338465872
		{ &Z_Construct_UFunction_USnakeGame_GameInstance_SaveRecord, "SaveRecord" }, // 2397743523
		{ &Z_Construct_UFunction_USnakeGame_GameInstance_SetNewRecord, "SetNewRecord" }, // 828136685
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USnakeGame_GameInstance_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SnakeGame_GameInstance.h" },
		{ "ModuleRelativePath", "Public/SnakeGame_GameInstance.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USnakeGame_GameInstance_Statics::NewProp_Record_MetaData[] = {
		{ "Category", "Save" },
		{ "ModuleRelativePath", "Public/SnakeGame_GameInstance.h" },
	};
#endif
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UClass_USnakeGame_GameInstance_Statics::NewProp_Record = { "Record", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(USnakeGame_GameInstance, Record), METADATA_PARAMS(Z_Construct_UClass_USnakeGame_GameInstance_Statics::NewProp_Record_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USnakeGame_GameInstance_Statics::NewProp_Record_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USnakeGame_GameInstance_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USnakeGame_GameInstance_Statics::NewProp_Record,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USnakeGame_GameInstance_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USnakeGame_GameInstance>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_USnakeGame_GameInstance_Statics::ClassParams = {
		&USnakeGame_GameInstance::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_USnakeGame_GameInstance_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_USnakeGame_GameInstance_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USnakeGame_GameInstance_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USnakeGame_GameInstance_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USnakeGame_GameInstance()
	{
		if (!Z_Registration_Info_UClass_USnakeGame_GameInstance.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_USnakeGame_GameInstance.OuterSingleton, Z_Construct_UClass_USnakeGame_GameInstance_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_USnakeGame_GameInstance.OuterSingleton;
	}
	template<> SNAKEGAME_API UClass* StaticClass<USnakeGame_GameInstance>()
	{
		return USnakeGame_GameInstance::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(USnakeGame_GameInstance);
	USnakeGame_GameInstance::~USnakeGame_GameInstance() {}
	struct Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_USnakeGame_GameInstance, USnakeGame_GameInstance::StaticClass, TEXT("USnakeGame_GameInstance"), &Z_Registration_Info_UClass_USnakeGame_GameInstance, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(USnakeGame_GameInstance), 245861462U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_3704998809(TEXT("/Script/SnakeGame"),
		Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_GameInstance_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
