// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "BonusSpeed.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_BonusSpeed_generated_h
#error "BonusSpeed.generated.h already included, missing '#pragma once' in BonusSpeed.h"
#endif
#define SNAKEGAME_BonusSpeed_generated_h

#define FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_15_SPARSE_DATA
#define FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_15_RPC_WRAPPERS
#define FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_15_ACCESSORS
#define FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABonusSpeed(); \
	friend struct Z_Construct_UClass_ABonusSpeed_Statics; \
public: \
	DECLARE_CLASS(ABonusSpeed, ABonusBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ABonusSpeed)


#define FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_15_INCLASS \
private: \
	static void StaticRegisterNativesABonusSpeed(); \
	friend struct Z_Construct_UClass_ABonusSpeed_Statics; \
public: \
	DECLARE_CLASS(ABonusSpeed, ABonusBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ABonusSpeed)


#define FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABonusSpeed(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABonusSpeed) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABonusSpeed); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABonusSpeed); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABonusSpeed(ABonusSpeed&&); \
	NO_API ABonusSpeed(const ABonusSpeed&); \
public: \
	NO_API virtual ~ABonusSpeed();


#define FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABonusSpeed() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABonusSpeed(ABonusSpeed&&); \
	NO_API ABonusSpeed(const ABonusSpeed&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABonusSpeed); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABonusSpeed); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABonusSpeed) \
	NO_API virtual ~ABonusSpeed();


#define FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_12_PROLOG
#define FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_15_SPARSE_DATA \
	FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_15_RPC_WRAPPERS \
	FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_15_ACCESSORS \
	FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_15_INCLASS \
	FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_15_SPARSE_DATA \
	FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_15_ACCESSORS \
	FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_15_INCLASS_NO_PURE_DECLS \
	FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class ABonusSpeed>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_SnakeGame_Source_SnakeGame_Public_BonusSpeed_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
