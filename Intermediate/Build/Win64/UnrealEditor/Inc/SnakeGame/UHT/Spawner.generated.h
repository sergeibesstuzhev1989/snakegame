// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Spawner.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_Spawner_generated_h
#error "Spawner.generated.h already included, missing '#pragma once' in Spawner.h"
#endif
#define SNAKEGAME_Spawner_generated_h

#define FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_19_SPARSE_DATA
#define FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSpawnBonus); \
	DECLARE_FUNCTION(execSpawnBlock);


#define FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSpawnBonus); \
	DECLARE_FUNCTION(execSpawnBlock);


#define FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_19_ACCESSORS
#define FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpawner(); \
	friend struct Z_Construct_UClass_ASpawner_Statics; \
public: \
	DECLARE_CLASS(ASpawner, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASpawner)


#define FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_19_INCLASS \
private: \
	static void StaticRegisterNativesASpawner(); \
	friend struct Z_Construct_UClass_ASpawner_Statics; \
public: \
	DECLARE_CLASS(ASpawner, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASpawner)


#define FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpawner(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpawner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawner); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawner(ASpawner&&); \
	NO_API ASpawner(const ASpawner&); \
public: \
	NO_API virtual ~ASpawner();


#define FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawner(ASpawner&&); \
	NO_API ASpawner(const ASpawner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawner); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawner); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpawner) \
	NO_API virtual ~ASpawner();


#define FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_16_PROLOG
#define FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_19_SPARSE_DATA \
	FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_19_RPC_WRAPPERS \
	FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_19_ACCESSORS \
	FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_19_INCLASS \
	FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_19_SPARSE_DATA \
	FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_19_ACCESSORS \
	FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_19_INCLASS_NO_PURE_DECLS \
	FID_SnakeGame_Source_SnakeGame_Public_Spawner_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class ASpawner>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_SnakeGame_Source_SnakeGame_Public_Spawner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
