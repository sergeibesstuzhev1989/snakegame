// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/Public/BonusBase.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBonusBase() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	SNAKEGAME_API UClass* Z_Construct_UClass_ABonusBase();
	SNAKEGAME_API UClass* Z_Construct_UClass_ABonusBase_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_ASnakeBase_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
	SNAKEGAME_API UFunction* Z_Construct_UDelegateFunction_SnakeGame_MyBaseDelegate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_SnakeGame_MyBaseDelegate__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_SnakeGame_MyBaseDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// \xd0\xbe\xd0\xb1\xd1\x8a\xd1\x8f\xd0\xb2\xd0\xbb\xd1\x8f\xd0\xb5\xd0\xbc \xd1\x81\xd0\xb2\xd0\xbe\xd0\xb9 \xd0\xb4\xd0\xb5\xd0\xbb\xd0\xb5\xd0\xb3\xd0\xb0\xd1\x82\n" },
		{ "ModuleRelativePath", "Public/BonusBase.h" },
		{ "ToolTip", "\xd0\xbe\xd0\xb1\xd1\x8a\xd1\x8f\xd0\xb2\xd0\xbb\xd1\x8f\xd0\xb5\xd0\xbc \xd1\x81\xd0\xb2\xd0\xbe\xd0\xb9 \xd0\xb4\xd0\xb5\xd0\xbb\xd0\xb5\xd0\xb3\xd0\xb0\xd1\x82" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_SnakeGame_MyBaseDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_SnakeGame, nullptr, "MyBaseDelegate__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_SnakeGame_MyBaseDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_SnakeGame_MyBaseDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_SnakeGame_MyBaseDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UDelegateFunction_SnakeGame_MyBaseDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(ABonusBase::execAutoDestroy)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AutoDestroy();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABonusBase::execBonusAction)
	{
		P_GET_OBJECT(ASnakeBase,Z_Param_Snake);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BonusAction(Z_Param_Snake);
		P_NATIVE_END;
	}
	void ABonusBase::StaticRegisterNativesABonusBase()
	{
		UClass* Class = ABonusBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AutoDestroy", &ABonusBase::execAutoDestroy },
			{ "BonusAction", &ABonusBase::execBonusAction },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ABonusBase_AutoDestroy_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABonusBase_AutoDestroy_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/BonusBase.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ABonusBase_AutoDestroy_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABonusBase, nullptr, "AutoDestroy", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABonusBase_AutoDestroy_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABonusBase_AutoDestroy_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABonusBase_AutoDestroy()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ABonusBase_AutoDestroy_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABonusBase_BonusAction_Statics
	{
		struct BonusBase_eventBonusAction_Parms
		{
			ASnakeBase* Snake;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Snake;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ABonusBase_BonusAction_Statics::NewProp_Snake = { "Snake", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(BonusBase_eventBonusAction_Parms, Snake), Z_Construct_UClass_ASnakeBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABonusBase_BonusAction_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABonusBase_BonusAction_Statics::NewProp_Snake,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABonusBase_BonusAction_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/BonusBase.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ABonusBase_BonusAction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABonusBase, nullptr, "BonusAction", nullptr, nullptr, sizeof(Z_Construct_UFunction_ABonusBase_BonusAction_Statics::BonusBase_eventBonusAction_Parms), Z_Construct_UFunction_ABonusBase_BonusAction_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABonusBase_BonusAction_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABonusBase_BonusAction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABonusBase_BonusAction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABonusBase_BonusAction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ABonusBase_BonusAction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ABonusBase);
	UClass* Z_Construct_UClass_ABonusBase_NoRegister()
	{
		return ABonusBase::StaticClass();
	}
	struct Z_Construct_UClass_ABonusBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UECodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABonusBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ABonusBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ABonusBase_AutoDestroy, "AutoDestroy" }, // 923046458
		{ &Z_Construct_UFunction_ABonusBase_BonusAction, "BonusAction" }, // 39253457
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABonusBase_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BonusBase.h" },
		{ "ModuleRelativePath", "Public/BonusBase.h" },
	};
#endif
		const UECodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ABonusBase_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ABonusBase, IInteractable), false },  // 2012260126
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABonusBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABonusBase>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ABonusBase_Statics::ClassParams = {
		&ABonusBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABonusBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABonusBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABonusBase()
	{
		if (!Z_Registration_Info_UClass_ABonusBase.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ABonusBase.OuterSingleton, Z_Construct_UClass_ABonusBase_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ABonusBase.OuterSingleton;
	}
	template<> SNAKEGAME_API UClass* StaticClass<ABonusBase>()
	{
		return ABonusBase::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABonusBase);
	ABonusBase::~ABonusBase() {}
	struct Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ABonusBase, ABonusBase::StaticClass, TEXT("ABonusBase"), &Z_Registration_Info_UClass_ABonusBase, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ABonusBase), 2553690176U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_3730146(TEXT("/Script/SnakeGame"),
		Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_BonusBase_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
