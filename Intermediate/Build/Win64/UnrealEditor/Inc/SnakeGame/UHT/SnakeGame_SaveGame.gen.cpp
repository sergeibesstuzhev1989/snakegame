// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/Public/SnakeGame_SaveGame.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSnakeGame_SaveGame() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_USaveGame();
	SNAKEGAME_API UClass* Z_Construct_UClass_USnakeGame_SaveGame();
	SNAKEGAME_API UClass* Z_Construct_UClass_USnakeGame_SaveGame_NoRegister();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
// End Cross Module References
	void USnakeGame_SaveGame::StaticRegisterNativesUSnakeGame_SaveGame()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(USnakeGame_SaveGame);
	UClass* Z_Construct_UClass_USnakeGame_SaveGame_NoRegister()
	{
		return USnakeGame_SaveGame::StaticClass();
	}
	struct Z_Construct_UClass_USnakeGame_SaveGame_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_SaveRecordScore_MetaData[];
#endif
		static const UECodeGen_Private::FIntPropertyParams NewProp_SaveRecordScore;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USnakeGame_SaveGame_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USaveGame,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USnakeGame_SaveGame_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "SnakeGame_SaveGame.h" },
		{ "ModuleRelativePath", "Public/SnakeGame_SaveGame.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USnakeGame_SaveGame_Statics::NewProp_SaveRecordScore_MetaData[] = {
		{ "Comment", "// UPROPERTY() \xd0\xbe\xd0\xb1\xd1\x8f\xd0\xb7\xd0\xb0\xd1\x82\xd0\xb5\xd0\xbb\xd0\xb5\xd0\xbd\n" },
		{ "ModuleRelativePath", "Public/SnakeGame_SaveGame.h" },
		{ "ToolTip", "UPROPERTY() \xd0\xbe\xd0\xb1\xd1\x8f\xd0\xb7\xd0\xb0\xd1\x82\xd0\xb5\xd0\xbb\xd0\xb5\xd0\xbd" },
	};
#endif
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UClass_USnakeGame_SaveGame_Statics::NewProp_SaveRecordScore = { "SaveRecordScore", nullptr, (EPropertyFlags)0x0010000000000000, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(USnakeGame_SaveGame, SaveRecordScore), METADATA_PARAMS(Z_Construct_UClass_USnakeGame_SaveGame_Statics::NewProp_SaveRecordScore_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USnakeGame_SaveGame_Statics::NewProp_SaveRecordScore_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USnakeGame_SaveGame_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USnakeGame_SaveGame_Statics::NewProp_SaveRecordScore,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USnakeGame_SaveGame_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USnakeGame_SaveGame>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_USnakeGame_SaveGame_Statics::ClassParams = {
		&USnakeGame_SaveGame::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USnakeGame_SaveGame_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USnakeGame_SaveGame_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USnakeGame_SaveGame_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USnakeGame_SaveGame_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USnakeGame_SaveGame()
	{
		if (!Z_Registration_Info_UClass_USnakeGame_SaveGame.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_USnakeGame_SaveGame.OuterSingleton, Z_Construct_UClass_USnakeGame_SaveGame_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_USnakeGame_SaveGame.OuterSingleton;
	}
	template<> SNAKEGAME_API UClass* StaticClass<USnakeGame_SaveGame>()
	{
		return USnakeGame_SaveGame::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(USnakeGame_SaveGame);
	USnakeGame_SaveGame::~USnakeGame_SaveGame() {}
	struct Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_USnakeGame_SaveGame, USnakeGame_SaveGame::StaticClass, TEXT("USnakeGame_SaveGame"), &Z_Registration_Info_UClass_USnakeGame_SaveGame, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(USnakeGame_SaveGame), 3921732048U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_3034873853(TEXT("/Script/SnakeGame"),
		Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_Public_SnakeGame_SaveGame_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
